<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="header.jsp" />

<h1>Knights Wanted for Unusual Quests</h1>

<div>
<p>
	The island of <strong>Generica</strong> has sent word to every city in the kingdom of <strong>Arpegia</strong> that
	it's facing various issues with the indigenous monsters and traders, and that only brave questing adventurers can
	help its development (somehow).
</p>
<p>
	Join the crowd and become a hero today! <a href="/register">Sign up</a> or <a href="/login">sign in</a> to join the fun!
</p>
<p>
	<strong>Caution:</strong> this game is not particularily entertaining.
</p>
</div>

<c:import url="footer.jsp" />