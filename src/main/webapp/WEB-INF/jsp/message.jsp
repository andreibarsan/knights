<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<h1>Messaging</h1>

<h2>Compose a message</h2>
<form name='messageForm' action="/message" method="post" role="form">
	<div class="form-group">
		<label for="user" class="form-label">Name:</label>
		<input type="text" name="user" id="user" class="form-control" />
	</div>

	<div class="form-group">
		<label for="msg" class="form-label">Message:</label> 
		<textarea name="msg" id="msg" class="form-control"></textarea>
	</div>

	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

	<button type="submit" role="button" class="btn btn-lg btn-primary">Send message!</button>
</form>

<h2>Received messages:</h2>

<c:choose>
	<c:when test="${messages.size() > 0}">
		<c:forEach var="m" items="${messages}">
			<div>From: <strong>${m.sender.name}</strong></div>
			<div>${m.msg}</div>
			<hr/>
		</c:forEach>
	</c:when>
	<c:otherwise>
		<div>No messages.</div>
	</c:otherwise>
</c:choose>