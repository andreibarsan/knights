<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="header.jsp" />

<h1>Roster of enemies</h1>

<table class="table table-striped">
<tr>
	<th>Name</th>
	<th>Level</th>
	<th>Health Points</th>
	<th>Attack</th>
	<th>Defense</th>
</tr>
<c:forEach var="e" items="${enemies}">
	<tr>
		<td>${e.name}</td>
		<td>${e.level}</td>
		<td>${e.max_health}</td>
		<td>${e.attack}</td>
		<td>${e.defense}</td>
	</tr>
</c:forEach>
</table>

<c:import url="footer.jsp" />