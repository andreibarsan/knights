<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="header.jsp" />

<h1>Arena fight</h1>
<c:choose>
<c:when test="${enemy != null}">
	<p>Fighting: ${enemy.name} (level ${enemy.level})</p>
	<c:import url="list-events.jsp" />
	
	<form action="/arena" method="post" role="form">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<a href="/dashboard"><button type="button" role="button" class="btn btn-default btn-lg">Back to dashboard!</button></a>
		<button type="submit" role="button" name="submit" class="btn btn-primary btn-lg">Fight again! (1 energy)</button>
	</form>
</c:when>
<c:otherwise>
	<p>No matching enemies found...</p>
</c:otherwise>
</c:choose>

<c:import url="footer.jsp" />