<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="header.jsp" />

<h1>Welcome to the shop</h1>

<table class="table table-striped">
<tr>
	<th>Name</th>
	<th>Bonuses</th>
	<th>Buy</th>
</tr>
<c:forEach var="i" items="${items}">
	<tr>
		<td><p>${i.name}</p><p class="small">${i.description}</p></td>
		<td>
			<c:if test="${i.attackbonus > 0}">
			+${i.attackbonus} Attack
			</c:if>
			<c:if test="${i.defensebonus > 0}">
			+${i.defensebonus} Defense
			</c:if>
			<c:if test="${i.healthbonus > 0}">
			+${i.healthbonus} Health
			</c:if>
		</td>
		<td>
			<form action="/store" method="post" role="form">
				<input type="hidden" name="item-id" value="${i.id}" />
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<button type="submit" class="btn btn-primary">Cost: ${i.value}</button>
			</form>
		</td>
	</tr>
</c:forEach>
</table>

<c:import url="footer.jsp" />