<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="../header.jsp" />

<h1>Whoa, your login seems to be invalid!</h1>
<h2>Your session probably expired or we restarted the server. Sorry about that! <a href="/login">Log in</a> to jump back into the action!</h2>

<c:import url="../footer.jsp" />