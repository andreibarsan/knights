<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Proiect Baze de Date Distribuite, IAG 7611, Semestrul VI">
<meta name="author" content="Andrei B�rsan and Sebastian Ghelase">
<link rel="shortcut icon" href="/assets/ico/favicon.ico">

<title>Knights Wanted for Unusual Quests</title>

<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="/assets/css/app.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	
	<c:url value="/logout" var="logoutUrl" />
	<c:url value="/login" var="loginUrl" />
	<c:url value="/register" var="registerUrl" />
	
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
				</button>
				
				<a class="navbar-brand" href="/">Knights Wanted for Unusual Quests</a>
			</div>
			
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav pull-right">
					<c:choose>
					<c:when test="${user != null}">	
						<li>
							<a href="/dashboard">
							<strong>${user.name}</strong> the ${user.characterClass.name}
							</a>
						</li>
						<li><a href="#">Level: ${user.level}</a></li>					
						<li><a href="#">Gold: ${user.gold}</a></li>
						<li><a href="#">Energy: ${user.energy}</a></li>
						<li><a href="/store">Store</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="/">Home</a></li>
					</c:otherwise>
					</c:choose>
					<li>
						<a href="/roster">Enemy roster</a>
					</li>
					<li>
					<c:choose>
					 	<c:when test="${pageContext.request.userPrincipal.name != null}">
							<form action="${logoutUrl}" method="post" id="logoutForm" class="navbar-form">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<button type="submit" role="submit" class="btn">Log out</button>
							</form>
						</c:when>
						<c:otherwise>
						<div class="navbar-form">
							<a href="${loginUrl}"><button class="btn btn-success">Sign in</button></a>
							<a href="${registerUrl}"><button class="btn btn-primary">Sign up</button></a>
						</div>
						</c:otherwise>
					</c:choose>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>

	<div class="container starter-template">
			<c:if test="${not empty success}">
				<div class="alert alert-success"><c:out value="${success}"></c:out></div>
			</c:if>
			<c:if test="${not empty danger}">
				<div class="alert alert-danger"><c:out value="${danger}"></c:out></div>
			</c:if>