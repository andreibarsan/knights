<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@ page session="true" %>

<c:import url="header.jsp"></c:import>

<h1>Login</h1>

<div id="login-box"> 
	<c:if test="${not empty error}">
		<div class="alert alert-danger">${error}</div>
	</c:if>
	<c:if test="${not empty msg}">
		<div class="alert alert-info">${msg}</div>
	</c:if>
	
	<c:url value="/j_spring_security_check" var="loginUrl" />
	
	<form name='loginForm' action="${loginUtl}" method="post" role="form">
			<div class="form-group">
				<label for="username" class="form-label">Name:</label>
				<input type="text" name="username" id="username" class="form-control" />
			</div>
			
			<div class="form-group">
				<label for="password" class="form-label">Password:</label>
				<input type="password" name="password" id="password" class="form-control" />
			</div>
		  
	  	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	  	
	  	<button type="submit" role="button" class="btn btn-lg btn-primary">Login!</button>
	</form>
</div>