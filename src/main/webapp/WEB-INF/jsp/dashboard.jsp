<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="header.jsp" />

<h1>Welcome to the user dashboard, <c:out value="${user.name}" />!</h1>
<table class="table">
	<tr><td>Gold:</td><td>${user.gold}</td></tr>
	<tr><td>Level:</td><td>${user.level}</td></tr>
	<tr><td>Class:</td><td>${user.characterClass.name}</td></tr>
	<tr>
		<td>Items:</td>
		<td>
			<c:choose>
				<c:when test="${user.items.size() > 0}">
					<ul class="list-unstyled">
					<c:forEach var="i" items="${user.items}">
						<li>${i.name}</li>
					</c:forEach>
					</ul>
				</c:when>
				<c:otherwise>
					none
				</c:otherwise>
			</c:choose>	
		</td>
	</tr>
</table>

<div>
	<h2>Actions</h2>
	
	<form action="arena" method="post" role="form">
		<div class="form-group">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" role="button" class="btn btn-primary btn-lg">Fight in the arena (1 Energy)</button>
		</div>
	</form>
	<form action="forest" method="post" role="form">
		<div class="form-group">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<button type="submit" role="button" class="btn btn-primary btn-lg">Explore the forest (5 Energy)</button>
		</div>
	</form>
</div>

<c:import url="message.jsp" />

<c:import url="footer.jsp" />