<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:forEach var="evt" items="${combat.events}">
	<c:choose>
		<c:when test="${evt.type == 'ROUND'}">
			<h3>${evt.text}</h3>
		</c:when>
		<c:when test="${evt.type == 'STORY'}">
			<p class="story">${evt.text}</p>
		</c:when>
		<c:when test="${evt.type == 'VICTORY'}">
			<h2 class="victory">${evt.text}</h2>
		</c:when>
		<c:when test="${evt.type == 'DEFEAT'}">
			<h2 class="defeat">${evt.text}</h2>
		</c:when>
		<c:when test="${evt.type == 'ENEMYHEAL' }">
			<p class="enemy-heal">${evt.text}</p>
		</c:when>
		<c:otherwise>
			<p>${evt.text}</p>
		</c:otherwise>
	</c:choose>
</c:forEach>