<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="header.jsp" />

<h1>Registration</h1>

<c:if test="${br.hasErrors()}">
	<div class="alert alert-danger">
	   <ul class="list-unstyled">
	      <c:forEach var="error" items="${br.allErrors}">
	         <li><spring:message code="${error.code}"></spring:message></li>
	      </c:forEach>
	   </ul>
	</div>
</c:if>

<form action="/register" method="post" role="form">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	  	
	<div class="form-group">
		<label for="name" class="form-label">Username:</label>
		<input type="text" name="name" id="name" class="form-control" /> 
	</div>
	
	<div class="form-group">
		<label for="password" class="form-label">Password:</label>
		<input type="password" name="password" id="password" class="form-control" /> 
	</div>
	
	<div class="form-group">
		<label for="passwordconf" class="form-label">Password confirmation:</label>
		<input type="password" name="passwordconf" id="passwordconf" class="form-control" /> 
	</div>
	
	<div class="form-group">
		<label for="characterclass" class="form-label">Class:</label>
		<select name="characterclass" id="characterclass" class="form-control">
			<c:forEach var="cls" items="${classes}">
				<option value="${cls.id}">${cls.name}</option>
			</c:forEach>
		</select>
	</div>

	<a href="/"></a><button type="button" class="btn btn-default btn-lg">Back</button></a>
	<button type="submit" name="submit" class="btn btn-primary btn-lg">Register!</button>
</form>

<c:import url="footer.jsp" />