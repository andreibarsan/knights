package com.siegedog.knights.models.mesaj;


import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("mesajDao")
public class MesajDaoImpl implements MesajDao {

	@Autowired  
    private SessionFactory sessionFactory;  
  
	@Override
	@Transactional
	public void save(Mesaj mesaj) {
		sessionFactory.getCurrentSession().saveOrUpdate(mesaj);
	}

	@Override
	@Transactional
	public void update(Mesaj mesaj) {
		sessionFactory.getCurrentSession().update(mesaj);
	}
	
	@Override
	@Transactional
	public void delete(Mesaj mesaj) {
		sessionFactory.getCurrentSession().delete(mesaj);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Mesaj> findAll() {
		return (List<Mesaj>) sessionFactory.getCurrentSession().createCriteria(Mesaj.class).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Mesaj> findByReceiver(Long receiver_id) {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(Mesaj.class)
				.add(Restrictions.eq("receiver.id", receiver_id))
				.list();
		return (List<Mesaj>) result;
	}


	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Mesaj> findByReceiver(String name) {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(Mesaj.class)
				.createAlias("receiver", "rec")
				.add(Restrictions.eq("rec.name", name))
				.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
				.list();
		return (List<Mesaj>) result;
	}
}
