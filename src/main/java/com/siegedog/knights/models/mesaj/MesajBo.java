package com.siegedog.knights.models.mesaj;

import java.util.List;

public interface MesajBo {
	void save(Mesaj mesaj);
	void update(Mesaj mesaj);
	void delete(Mesaj mesaj);
	List<Mesaj> findAll();
	List<Mesaj> findByReceiver(Long receiver_id);
	List<Mesaj> findByReceiver(String name);
}
