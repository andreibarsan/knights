package com.siegedog.knights.models.mesaj;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service  
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)  
public class MesajBoImpl implements MesajBo {

	@Autowired
	MesajDao mesajDao;
	
	@Override
	public void save(Mesaj mesaj) {
		mesajDao.save(mesaj);
	}

	@Override
	public void update(Mesaj mesaj) {
		mesajDao.update(mesaj);
	}

	@Override
	public void delete(Mesaj mesaj) {
		mesajDao.delete(mesaj);
	}

	@Override
	public List<Mesaj> findAll() {
		return mesajDao.findAll();
	}

	@Override
	public List<Mesaj> findByReceiver(Long receiver_id) {
		return mesajDao.findByReceiver(receiver_id);
	}

	@Override
	public List<Mesaj> findByReceiver(String name) {
		return mesajDao.findByReceiver(name);
	}
}
