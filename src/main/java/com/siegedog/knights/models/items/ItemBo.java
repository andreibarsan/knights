package com.siegedog.knights.models.items;

import java.util.List;

public interface ItemBo {
	List<Item> findAll();
	Item findById(Long id);
}
