package com.siegedog.knights.models.items;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("itemDao")
public class ItemDaoImpl implements ItemDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Item> findAll() {
		return (List<Item>) sessionFactory
				.getCurrentSession()
				.createCriteria(Item.class).list();
	}

	@Override
	@Transactional
	public Item findById(Long id) {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(Item.class).add(Restrictions.eq("id", id))
				.setMaxResults(1).list();

		if (0 == result.size()) {
			return null;
		}

		return (Item) result.get(0);
	}

}
