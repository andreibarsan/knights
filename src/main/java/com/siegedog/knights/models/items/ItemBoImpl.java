package com.siegedog.knights.models.items;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service  
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false)
public class ItemBoImpl implements ItemBo {

	@Autowired
	ItemDao itemDao;
	
	@Override
	public List<Item> findAll() {
		return itemDao.findAll();
	}

	@Override
	public Item findById(Long id) {
		return itemDao.findById(id);
	}

}
