package com.siegedog.knights.models.items;

import java.util.List;

public interface ItemDao {
	List<Item> findAll();
	Item findById(Long id);
}
