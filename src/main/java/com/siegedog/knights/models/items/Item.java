package com.siegedog.knights.models.items;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "items")
public class Item {

	@Id
	@GeneratedValue
	Long id;

	private String name;
	private String description;
	private String icon;

	private Integer level;
	private Integer value;

	private Integer attackbonus;
	private Integer defensebonus;
	private Integer healthbonus;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getAttackbonus() {
		return attackbonus;
	}

	public void setAttackbonus(Integer attackbonus) {
		this.attackbonus = attackbonus;
	}

	public Integer getDefensebonus() {
		return defensebonus;
	}

	public void setDefensebonus(Integer defensebonus) {
		this.defensebonus = defensebonus;
	}

	public Integer getHealthbonus() {
		return healthbonus;
	}

	public void setHealthbonus(Integer healthbonus) {
		this.healthbonus = healthbonus;
	}

	public Long getId() {
		return id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(! (obj instanceof Item)) {
			return false;
		}
		
		 Item other = (Item) obj;
		 return other.getName().equals(getName()); 
	}
	
	@Override
	public int hashCode() {
		return getName().hashCode();
	}
}
