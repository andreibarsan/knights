package com.siegedog.knights.models.user;

import java.util.List;

public interface UserDao {
	void save(User user);

	void update(User user);

	void delete(User user);

	User findById(Long id);

	User findByName(String string); 
	
	List<User> findAll();
}
