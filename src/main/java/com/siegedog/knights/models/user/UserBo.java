package com.siegedog.knights.models.user;

import java.util.List;

public interface UserBo {
	void save(User user);
	void update(User user);
	void delete(User user);
	User findById(Long id);
	User findByName(String name);
	List<User> findAll();
}
