package com.siegedog.knights.models.user;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.siegedog.knights.gameplay.CombatEntity;
import com.siegedog.knights.models.characterclass.CharacterClass;
import com.siegedog.knights.models.items.Item;

@Entity
@Table(name = "users")
public class User implements CombatEntity {
	private static int[] LEVELS = new int[] {
		0,
		0,
		50,
		120,
		200,
		300,
		450,
		600,
		800,
		1100,
		1500,
		2000,
		2500,
		3000,
		3500,
		4000,
		4500,
		5000,
		6000,
		7000,
		8000,
		9000
	};
	
	@Id @GeneratedValue
	private Long id;
	
	private String name;
	private String password_hash;
	private String role;
	private Date registered_date;

	private Integer energy;
	private Integer gold;
	private Integer xp;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(
		name = "items_users", 
		joinColumns = {	@JoinColumn(name = "id_user", nullable = false, updatable = true) }, 
		inverseJoinColumns = { @JoinColumn(name = "id_item", nullable = false, updatable = true) })
	private Set<Item> items;
	
	@ManyToOne
	@JoinColumn(name = "class", referencedColumnName = "id")
	private CharacterClass characterClass;
	
	// Used in combat simulations
	@Transient
	private Integer currentHealth;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getPassword_hash() {
		return password_hash;
	}

	public void setPassword_hash(String password_hash) {
		this.password_hash = password_hash;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getEnergy() {
		return energy;
	}

	public void setEnergy(Integer energy) {
		this.energy = energy;
	}

	public Integer getGold() {
		return gold;
	}

	public void setGold(Integer gold) {
		this.gold = gold;
	}

	public Integer getXp() {
		return xp;
	}

	public void setXp(Integer xp) {
		this.xp = xp;
	}

	public Long getId() {
		return id;
	}

	public Date getRegistered_date() {
		return registered_date;
	}
	
	public CharacterClass getCharacterClass() {
		return characterClass;
	}

	public void setCharacterClass(CharacterClass characterClass) {
		this.characterClass = characterClass;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public int getLevel() {
		int lvl = Arrays.binarySearch(LEVELS, getXp());
		if(lvl < 0) {
			lvl = (-lvl) - 1;
		}
		return lvl;
	}
	
	public Integer getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(Integer currentHealth) {
		this.currentHealth = currentHealth;
	}
	
	public Integer getMaxHealth() {
		int lvl = getLevel();
		return characterClass.getBase_health() + lvl * characterClass.getLvl_health();
	}
	
	public Integer getAttack() {
		int lvl = getLevel();
		return characterClass.getBase_attack() + lvl * characterClass.getLvl_attack();
	}
	
	public Integer getDefense() {
		int lvl = getLevel();
		return characterClass.getBase_defense() + lvl * characterClass.getLvl_defense();
	}

	@Override
	public void initCombat() {
		currentHealth = getMaxHealth();
	}
}
