package com.siegedog.knights.models.user;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("userDao")
public class UserDaoImpl implements UserDao {

	@Autowired  
    private SessionFactory sessionFactory;  
  
	@Override
	@Transactional
	public void save(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Override
	@Transactional
	public void update(User user) {
		sessionFactory.getCurrentSession().update(user);
	}
	
	@Override
	@Transactional
	public void delete(User user) {
		sessionFactory.getCurrentSession().delete(user);
	}

	@Override
	@Transactional
	public User findById(Long id) {
		List<?> result = sessionFactory.getCurrentSession().createCriteria(User.class)
							.add(Restrictions.eq("id", id)).setMaxResults(1).list();
		
		if(0 == result.size()) {
			return null;
		}
		
		return (User) result.get(0);
	}

	@Override
	@Transactional
	public User findByName(String name) {
		List<?> result = sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("name", name)).list();
		
		if(0 == result.size()) {
			return null;
		}
		
		return (User) result.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<User> findAll() {
		return (List<User>) sessionFactory.getCurrentSession().createCriteria(User.class).list();
	}

}