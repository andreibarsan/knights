package com.siegedog.knights.models.characterclass;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CharacterClassBoImpl implements CharacterClassBo {

	@Autowired
	CharacterClassDao characterClassDao;

	@Override
	public CharacterClass findById(Long id) {
		return characterClassDao.findById(id);
	}

	@Override
	public List<CharacterClass> findAll() {
		return characterClassDao.findAll();
	}
}
