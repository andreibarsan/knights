package com.siegedog.knights.models.characterclass;

import java.util.List;

public interface CharacterClassDao {

	CharacterClass findById(Long id);
	List<CharacterClass> findAll();
}
