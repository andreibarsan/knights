package com.siegedog.knights.models.characterclass;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "classes")
public class CharacterClass {

	@Id	@GeneratedValue
	private Long id;

	private String name;
	private Integer base_attack;
	private Integer lvl_attack;
	private Integer base_defense;
	private Integer lvl_defense;
	private Integer base_health;
	private Integer lvl_health;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBase_attack() {
		return base_attack;
	}

	public void setBase_attack(Integer base_attack) {
		this.base_attack = base_attack;
	}

	public Integer getLvl_attack() {
		return lvl_attack;
	}

	public void setLvl_attack(Integer lvl_attack) {
		this.lvl_attack = lvl_attack;
	}

	public Integer getBase_defense() {
		return base_defense;
	}

	public void setBase_defense(Integer base_defense) {
		this.base_defense = base_defense;
	}

	public Integer getLvl_defense() {
		return lvl_defense;
	}

	public void setLvl_defense(Integer lvl_defense) {
		this.lvl_defense = lvl_defense;
	}

	public Integer getBase_health() {
		return base_health;
	}

	public void setBase_health(Integer base_health) {
		this.base_health = base_health;
	}

	public Integer getLvl_health() {
		return lvl_health;
	}

	public void setLvl_health(Integer lvl_health) {
		this.lvl_health = lvl_health;
	}

	public Long getId() {
		return id;
	}
}
