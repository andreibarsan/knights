package com.siegedog.knights.models.characterclass;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("characterClassDao")
public class CharacterClassDaoImpl implements CharacterClassDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@Transactional
	public CharacterClass findById(Long id) {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(CharacterClass.class)
				.add(Restrictions.eq("id", id))
				.setMaxResults(1)
				.list();
		
		if(result.isEmpty()) {
			return null;
		}
		
		return (CharacterClass) result.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CharacterClass> findAll() {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(CharacterClass.class)
				.list();
		
		return (List<CharacterClass>) result;
	}

}
