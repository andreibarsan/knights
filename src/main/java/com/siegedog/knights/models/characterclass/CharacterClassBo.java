package com.siegedog.knights.models.characterclass;

import java.util.List;

public interface CharacterClassBo {
	CharacterClass findById(Long id);
	List<CharacterClass> findAll();
}
