package com.siegedog.knights.models.enemy;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("enemyDao")
public class EnemyDaoImpl implements EnemyDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public Enemy findById(Long id) {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(Enemy.class)
				.add(Restrictions.eq("id", id))
				.setMaxResults(1).list();

		if (0 == result.size()) {
			return null;
		}
		return (Enemy) result.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Enemy> findAll() {
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(Enemy.class)
				.list();

		return (List<Enemy>) result;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Enemy> findInLevelRange(int center, int variation) {
		int left = center - variation;
		int right = center + variation;
		
		List<?> result = sessionFactory.getCurrentSession()
				.createCriteria(Enemy.class)
				.add(Restrictions.ge("level", left))
				.add(Restrictions.le("level", right))
				.list();

		return (List<Enemy>) result;
	}
}
