package com.siegedog.knights.models.enemy;

import java.util.List;

public interface EnemyBo {
	Enemy findById(Long id);
	List<Enemy> findAll();
	List<Enemy> findInLevelRange(int center, int variation);
}
