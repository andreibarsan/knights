package com.siegedog.knights.models.enemy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EnemyBoImpl implements EnemyBo {

	@Autowired
	EnemyDao enemyDao;
	
	@Override
	public Enemy findById(Long id) {
		return enemyDao.findById(id);
	}

	@Override
	public List<Enemy> findAll() {
		return enemyDao.findAll();
	}

	@Override
	public List<Enemy> findInLevelRange(int center, int variation) {
		return enemyDao.findInLevelRange(center, variation);
	}
}
