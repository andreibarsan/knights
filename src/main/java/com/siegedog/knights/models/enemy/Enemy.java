package com.siegedog.knights.models.enemy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.siegedog.knights.gameplay.CombatEntity;

@Entity
@Table(name = "enemies")
public class Enemy implements CombatEntity {
	@Id
	@GeneratedValue
	private Long id;

	private String name;
	private Integer level;
	private Integer max_health;
	private Integer attack;
	private Integer defense;
	
	/**
	 * Whether this enemy has the ability to heal itself. The level dictates
	 * the amount. The enemy sacrifices a turn to heal itself significantly.
	 */
	private Integer heal;
	
	/**
	 * Attacking has a chance to weaken the target, causing it to deal reduced
	 * damage.
	 */
	private Integer weakness;
	
	/**
	 * Has a chance to fully block an attack, mitigating all damage.
	 */
	private Integer block;
	
	@Transient private Integer currentHealth; 

	@Override
	public void initCombat() {
		currentHealth = max_health;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getMax_health() {
		return max_health;
	}

	public void setMax_health(Integer max_health) {
		this.max_health = max_health;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		this.defense = defense;
	}

	public Long getId() {
		return id;
	}

	public Integer getHeal() {
		return heal;
	}

	public void setHeal(Integer heal) {
		this.heal = heal;
	}

	public Integer getWeakness() {
		return weakness;
	}

	public void setWeakness(Integer weakness) {
		this.weakness = weakness;
	}

	public Integer getBlock() {
		return block;
	}

	public void setBlock(Integer block) {
		this.block = block;
	}
	
	public Integer getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(Integer currentHealth) {
		this.currentHealth = currentHealth;
	}
}