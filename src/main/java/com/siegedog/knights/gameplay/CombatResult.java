package com.siegedog.knights.gameplay;

import java.util.ArrayList;
import java.util.List;

public class CombatResult {
	private List<CombatEvent> events = new ArrayList<CombatEvent>();
	private boolean playerWon;
	
	public void addRound(String format, Object... args) {
		events.add(new CombatEvent(EventType.ROUND, String.format(format, args)));
	}
	
	public void addAttack(String format, Object... args) {
		events.add(new CombatEvent(EventType.ATTACK, String.format(format, args)));
	}
	
	public void addDeath(String format, Object... args) {
		events.add(new CombatEvent(EventType.DEATH, String.format(format, args)));
	}
	
	public void addVictory(String format, Object... args) {
		events.add(new CombatEvent(EventType.VICTORY, String.format(format, args)));
	}
	
	public void addDefeat(String format, Object... args) {
		events.add(new CombatEvent(EventType.DEFEAT, String.format(format, args)));
	}
	
	public void addStory(String format, Object... args) {
		events.add(new CombatEvent(EventType.STORY, String.format(format, args)));
	}
	
	public List<CombatEvent> getEvents() {
		return events;
	}
	
	public void setPlayerWon(boolean playerWon) {
		this.playerWon = playerWon;
	}
	
	public boolean isPlayerWon() {
		return playerWon;
	}

	public void addEnemyHeal(String format, Object... args) {
		events.add(new CombatEvent(EventType.ENEMYHEAL, String.format(format, args)));
	}
	
	public void addEnemySpell(String format, Object... args) {
		events.add(new CombatEvent(EventType.ENEMYSPELL, String.format(format, args)));
	}
}