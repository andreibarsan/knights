package com.siegedog.knights.gameplay;

public enum EventType {
	ROUND,
	ATTACK,
	DEATH,
	VICTORY,
	DEFEAT,
	STORY,
	
	ENEMYHEAL,
	ENEMYSPELL
}