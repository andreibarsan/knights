package com.siegedog.knights.gameplay;

public interface CombatEntity {

	/**
	 * Prepare this entity for combat (e.g. set its current health to the value
	 * of its maximum health).
	 */
	public void initCombat();
}
