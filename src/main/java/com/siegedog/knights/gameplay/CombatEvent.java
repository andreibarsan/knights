package com.siegedog.knights.gameplay;

import com.siegedog.knights.controller.GameplayController;

public class CombatEvent {
	private EventType type;
	private String text;
	
	public CombatEvent(EventType type, String text) {
		this.type = type;
		this.text = text;
	}
	
	public EventType getType() {
		return type;
	}
	
	public void setType(EventType type) {
		this.type = type;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}		
}