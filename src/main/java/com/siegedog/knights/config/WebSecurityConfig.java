package com.siegedog.knights.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

@Configuration
@EnableWebMvcSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	DataSource dataSource;

	/**
	 * Important: csrf prevention is on by default.
	 */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/dashboard").hasRole("USER")
                .antMatchers("/forest").hasRole("USER")
                .antMatchers("/arena").hasRole("USER")
                .antMatchers("/store").hasRole("USER")
                .anyRequest().permitAll();
        
        http
        	.formLogin().loginPage("/login").failureUrl("/login?error").defaultSuccessUrl("/dashboard")
            .permitAll()
        .and()
            .logout()
            .permitAll()
        .and()
        	.exceptionHandling().accessDeniedPage("/403");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	auth.jdbcAuthentication().dataSource(dataSource)
			.usersByUsernameQuery("select name, password_hash, enabled from users where name=?")
			// Hash passwords with sha-256
			.passwordEncoder(new ShaPasswordEncoder(256))
			.authoritiesByUsernameQuery("select name, role from users where name=?");
    }
}
