package com.siegedog.knights.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/** Required to inject the proper security filter. */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}