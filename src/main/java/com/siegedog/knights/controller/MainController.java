package com.siegedog.knights.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.siegedog.knights.helpers.UserHelper;
import com.siegedog.knights.helpers.UserRegistration;
import com.siegedog.knights.helpers.UserRegistrationValidator;
import com.siegedog.knights.models.characterclass.CharacterClassBo;
import com.siegedog.knights.models.enemy.EnemyBo;
import com.siegedog.knights.models.mesaj.Mesaj;
import com.siegedog.knights.models.mesaj.MesajBo;
import com.siegedog.knights.models.user.User;
import com.siegedog.knights.models.user.UserBo;

@Controller
public class MainController {

	@Autowired UserBo userBo;
	@Autowired MesajBo mesajBo;
	@Autowired UserRegistrationValidator registrationValidator;
	@Autowired CharacterClassBo characterClassBo;
	@Autowired EnemyBo enemyBo;
	
	@RequestMapping("/")
	public String welcome(Map<String, Object> model) {
		return "home";
	}
	
	@RequestMapping(value = "/dashboard**", method = RequestMethod.GET)
	public ModelAndView dashboard() { 
		ModelAndView model = new ModelAndView();
		model.setViewName("dashboard");
		User user = UserHelper.getCurrentUser(userBo);
		model.addObject("user", user);
		model.addObject("messages", mesajBo.findByReceiver(user.getName()));
		return model;
	}
	
	@RequestMapping(value = "/message", method = RequestMethod.POST)
	public String sendMessage(
			@RequestParam(value = "user") String username,
			@RequestParam(value = "msg") String msg,
			final RedirectAttributes redirectAttributes
			) {
		
		User target = userBo.findByName(username);
		if(null == target) {
			redirectAttributes.addFlashAttribute("danger", "That user does not exist.");
		}
		else {
			Mesaj m = new Mesaj();
			m.setMsg(msg);
			m.setReceiver(target);
			m.setSender(UserHelper.getCurrentUser(userBo));
			mesajBo.save(m);
			
			redirectAttributes.addFlashAttribute("success", "Message sent!");
		}
		
		return "redirect:/dashboard";
	}

	@RequestMapping(value = "/roster", method = RequestMethod.GET)
	public ModelAndView roster() {
		ModelAndView model = new ModelAndView();
		if(UserHelper.isLoggedIn()) {
			model.addObject("user", UserHelper.getCurrentUser(userBo));
		}
		model.addObject("enemies", enemyBo.findAll());
		model.setViewName("roster");
		return model;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getRegister(Model model) {
		model.addAttribute("classes", characterClassBo.findAll());
		return "register";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String postRegister(
			Model model,
			@ModelAttribute("ur") UserRegistration ur,
			BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) {
		
		registrationValidator.validate(ur, bindingResult);
		
		if(bindingResult.hasErrors()) {
			model.addAttribute("classes", characterClassBo.findAll());
			model.addAttribute("br", bindingResult);
			return "register";
		}
		
		// No errors - transfer data to the main user model and save it
		User user = new User();
		user.setName(ur.getName());
		user.setPassword_hash(new ShaPasswordEncoder(256).encodePassword(ur.getPassword(), null));
		user.setRole("ROLE_USER");
		user.setEnergy(10);
		user.setGold(500);
		user.setXp(0);
		user.setCharacterClass(characterClassBo.findById(ur.getCharacterclass()));
		userBo.save(user);
		
		redirectAttributes.addFlashAttribute("success", "Account created successfully! Log in to start using the site!");
		
		return "redirect:/login";
	}
	
	@RequestMapping(value = "/403") 
	public String forbiddenError() {
		return "/errors/403";
	}
}
