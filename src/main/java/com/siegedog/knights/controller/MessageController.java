package com.siegedog.knights.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.siegedog.knights.models.mesaj.MesajBo;
import com.siegedog.knights.models.user.UserBo;

@Controller
public class MessageController {

	@Autowired MesajBo mesajBo;
	
	@RequestMapping(value = "/message", method = RequestMethod.POST)
	public ModelAndView showMessages(Model model) {
		System.out.println();
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("message");
		modelAndView.addObject("messages", "mess!");
		System.out.println(mesajBo.findAll().toArray().toString());
		return modelAndView;
	}
	

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView sendMessage(ModelAndView modelAndView) {
		System.out.println();
		modelAndView.setViewName("message");
		modelAndView.addObject("messages", "mess!");
		System.out.println(mesajBo.findAll().toArray().toString());
		return modelAndView;
	}
}