package com.siegedog.knights.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.siegedog.knights.gameplay.CombatResult;
import com.siegedog.knights.helpers.UserHelper;
import com.siegedog.knights.models.characterclass.CharacterClassBo;
import com.siegedog.knights.models.enemy.Enemy;
import com.siegedog.knights.models.enemy.EnemyBo;
import com.siegedog.knights.models.items.Item;
import com.siegedog.knights.models.items.ItemBo;
import com.siegedog.knights.models.user.User;
import com.siegedog.knights.models.user.UserBo;

@Controller
public class GameplayController {

	private static final float GOLD_FACTOR 	= 20.0f;
	private static final float XP_FACTOR 	= 10.0f;
	private static final int ARENA_COST 	= 1;
	private static final int FOREST_COST 	= 5;
	private static final int MAX_ROUNDS 	= 25;
	
	@Autowired UserBo userBo;
	@Autowired CharacterClassBo characterClassBo;
	@Autowired EnemyBo enemyBo;
	@Autowired ItemBo itemBo;
	
	@RequestMapping(value = "/store", method = RequestMethod.GET)
	public ModelAndView storeGet() {
		ModelAndView model = new ModelAndView();
		model.addObject("items", itemBo.findAll());
		model.addObject("user", UserHelper.getCurrentUser(userBo));
		model.setViewName("store");
		return model;
	}
	
	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public String buyItem(@RequestParam("item-id") Long itemId, RedirectAttributes redirectAttributes) {
		Item newItem = itemBo.findById(itemId);
		User u = UserHelper.getCurrentUser(userBo);
		
		if(null != newItem) {
			if(u.getItems().contains(newItem)) {
				redirectAttributes.addFlashAttribute("danger", "You already have that item!");
			}
			else if(u.getGold() >= newItem.getValue()) {
				u.setGold(u.getGold() - newItem.getValue());
				u.getItems().add(newItem);
				userBo.update(u);
				
				redirectAttributes.addFlashAttribute("success", "Bought the " + newItem.getName() + "!");
			}
			else {			
				redirectAttributes.addFlashAttribute("danger", "Not enough money. Get a job, you lousy hippie!");
			}
		}
		else {
			redirectAttributes.addFlashAttribute("danger", "That item does not exist.");
		}
		
		return "redirect:/store";		
	}
	
	@RequestMapping(value = "/arena", method = RequestMethod.GET)
	public String arenaGet() {
		return "redirect:/dashboard";
	}
	
	@RequestMapping(value = "/arena", method = RequestMethod.POST)
	public ModelAndView arena(final RedirectAttributes redirectAttributes) {
		ModelAndView model = new ModelAndView();
		User user = UserHelper.getCurrentUser(userBo);
		if(user.getEnergy() >= ARENA_COST) {
			user.setEnergy(user.getEnergy() - ARENA_COST);
			Enemy enemy = getAppropriateEnemy(user);
			
			if(null != enemy) {
				model.addObject("enemy", enemy);
				CombatResult cr = new CombatResult();
				resolveArena(cr, user, enemy);
				userBo.update(user);
				model.addObject("combat", cr);
			}
		}
		else {
			redirectAttributes.addFlashAttribute("danger", "Not enough energy. Try again later!");
			return new ModelAndView("redirect:/dashboard");
		}
		
		model.addObject("user", user);
		model.setViewName("arena");
		return model;
	}
	
	@RequestMapping(value = "/forest", method = RequestMethod.GET)
	public String forestGet() {
		return "redirect:/dashboard";
	}
	
	@RequestMapping(value = "/forest", method = RequestMethod.POST)
	public ModelAndView forest(final RedirectAttributes redirectAttributes) {
		ModelAndView model = new ModelAndView();
		User user = UserHelper.getCurrentUser(userBo);
		
		if(user.getEnergy() >= FOREST_COST) {
			user.setEnergy(user.getEnergy() - FOREST_COST);
			
			CombatResult cr = new CombatResult();
			resolveForest(cr, user);
			userBo.update(user);
			model.addObject("combat", cr);			
		}
		else {
			redirectAttributes.addFlashAttribute("danger", "Not enough energy. Try again later!");
			return new ModelAndView("redirect:/dashboard");
		}
		
		model.addObject("user", user);
		model.setViewName("forest");
		return model;
	}
	
	private void resolveForest(CombatResult cr, User user) {
		Random r = new Random();
		int minorEvents = 4 + r.nextInt(2);
		int fightEvents = 2 + r.nextInt(2);
		int majorEvents = 0;
		
		if(r.nextFloat() <= 0.1f) {
			majorEvents += 1;
		}
		
		cr.addRound("You set off on your quest through the forest!");
		cr.addStory("The weather is warm and sunny, and the forest actually looks inviting. You're excited about exploring it!");
		cr.addStory("You enter the forest and start following a path.");
		
		// Setup the player stats for combat simulations
		user.initCombat();
		while(true) {
			boolean eventHappened = false;
			
			while(!eventHappened) {
				if(r.nextFloat() <= 0.5f) {
					if(minorEvents > 0) {
						minorEvents--;
						// Trigger minor event
						resolveMinorEvent(cr, user);
						eventHappened = true;
					}
				}
				else if(r.nextFloat() < 0.8) {
					if(fightEvents > 0) {
						fightEvents--;
						// Trigger combat
						
						Enemy enemy = getAppropriateEnemy(user);
						enemy.initCombat();
						
						cr.addStory("You are strolling through the forest, when, all of a sudden, a <b>%s</b> appears out of nowhere and attacks you. %s",
							enemy.getName(),
							(r.nextFloat() > 0.75f) ? "It looks pissed!" : ""
						);
						
						resolveCombat(cr, user, enemy);
						eventHappened = true;
						
						// Heal after combat to make the game fair
						// Unless you're dead.
						if(cr.isPlayerWon() && user.getCurrentHealth() < user.getMaxHealth()) {
							int dmg = user.getMaxHealth() - user.getCurrentHealth();
							int maxHeal = (int) (0.33f * user.getMaxHealth());
							
							int heal = Math.min(maxHeal, dmg);
							user.setCurrentHealth(user.getCurrentHealth() + heal);
							
							cr.addStory("You manage to patch yourself up, restoring <b>%d</b> health.", heal);
							cr.addStory("You are now at %d/%d health.", user.getCurrentHealth(), user.getMaxHealth());
						}
					}
				}
				else {
					if(majorEvents > 0) {
						majorEvents--;
						// Trigger major event
						resolveMajorEvent(cr, user);
						eventHappened = true;
					}
				}
			}			
			// FIXME: this works right now, but bear in mind that this will keep
			// getting reset every time resolveCombat is invoked!
			if(user.getCurrentHealth() <= 0) {
				cr.addStory("A helpful elf carries you to the edge of the forest where a bored healer picks you up and binds some of your wounds while eating a chocolare bar.");
				cr.setPlayerWon(false);
				break;
			}
			
			if(minorEvents == 0 && majorEvents == 0 && fightEvents == 0) {
				cr.addStory("You find your way out of the forest, (relatively) unscathed!");
				cr.setPlayerWon(true);
				break;
			}
			
			cr.addRound("You continue your quest through the forest...");
		}
	}

	private void resolveArena(CombatResult cr, User user, Enemy enemy) {
		user.initCombat();
		enemy.initCombat();
		
		resolveCombat(cr, user, enemy);
		
		if(cr.isPlayerWon()) {
			cr.addStory("You leave the arena and an old healer binds your wounds.");
		}
		else {
			cr.addStory("An unskilled healer patches you up but leaves out a few injuries.");
			cr.addStory("You feel like shit, but at least you're not dead.");
		}
	}

	private void resolveCombat(CombatResult cr, User user, Enemy enemy) {
		Random r = new Random();
		String ename = enemy.getName();
		
		int round = 1;
		int lastHeal = 1;
		
		while(true) {
			cr.addRound("Round %d", round);
			cr.addStory("You: %d HP | %s: %d HP", user.getCurrentHealth(), ename, enemy.getCurrentHealth());
			
			int userDmg = computeEffectiveDamage(user.getAttack(), enemy.getDefense(), 0.2f);
			
			cr.addAttack("You hit <b>%s</b> for <b>%d</b> damage!", ename, userDmg);
			enemy.setCurrentHealth(enemy.getCurrentHealth() - userDmg);
			
			if(enemy.getCurrentHealth() <= 0) {
				// The enemy has died from your attack
				cr.addDeath("%s dies from the attack.", ename);
			}
			else {
				// The enemy will react
				
				// Attempt to heal
				float hpp = enemy.getCurrentHealth() / (float) enemy.getMax_health();
				if(enemy.getHeal() > 0 && hpp < 0.33f && round - lastHeal > 3) {
					lastHeal = round;
					int heal = 75 + enemy.getHeal() * 35;
					enemy.setCurrentHealth(enemy.getCurrentHealth() + heal);
					cr.addEnemyHeal("The %s casts heal! It heals itself for %d health points.", ename, heal);
					cr.addStory("The %s is at %d/%d health.", ename, enemy.getCurrentHealth(), enemy.getMax_health());
				}
				else {
					// The enemy will strike back
					int enemyDmg = computeEffectiveDamage(enemy.getAttack(), user.getDefense(), 0.2f);
					enemyDmg = Math.max(1, enemyDmg - user.getDefense());
					cr.addAttack("<b>%s</b> hits you for <b>%d</b> damage!", ename, enemyDmg);
					user.setCurrentHealth(user.getCurrentHealth() - enemyDmg);
					
					if(user.getCurrentHealth() <= 0) {
						cr.addDeath("You have died from the <b>%s's</b> attack...", ename);
					}
				}
			}
			
			if(user.getCurrentHealth() <= 0) {
				cr.setPlayerWon(false);
				break;
			}
			else if(enemy.getCurrentHealth() <= 0) {
				cr.setPlayerWon(true);
				break;
			}
			
			round++;
			if(round > MAX_ROUNDS) {
				cr.addStory("After %d rounds, you decided to call it a day and go get burgers.", MAX_ROUNDS);
				break;
			}
		}
		
		if(cr.isPlayerWon()) {
			cr.addVictory("Victory!");
			cr.addStory("Time to reap the spoils of victory!");
			int el = enemy.getLevel();
			
			int gold = (int) (5 + GOLD_FACTOR * el * (0.75 + r.nextDouble() * 0.5));
			int xp = (int) (10 + XP_FACTOR * el * (0.9 + r.nextDouble() * 0.2));
			
			user.setGold(user.getGold() + gold);
			cr.addStory("You won %d gold.", gold);
			
			grantXP(cr, user, xp);
		}
		else {
			cr.addDefeat("Defeat!");
		}
	}
	
	private int computeEffectiveDamage(int atk, int def, float variation) {
		float v_b = 1 - variation / 2.0f;
		int dmg = (int) (atk * (v_b + Math.random() * variation));
		
		// Diminishing returns for armors
		// Approaches 1 (100%) asymptotically
		float mitigation = def / (def + 200.0f);
		return Math.max(1, (int) (dmg * (1.0f - mitigation)));
	}

	private void resolveMinorEvent(CombatResult cr, User user) {
		Random r = new Random();
		float roll = r.nextFloat();
		
		// Assume that the player won't be killed by a minor event
		cr.setPlayerWon(true);
		
		if(roll < 0.2f) {
			cr.addStory("You stumble upon a small clearing. ");
			cr.addStory("Even though there isn't any action here, you enjoy the break.");
			if(user.getCurrentHealth() < user.getMaxHealth()) {
				int heal = Math.min(30, user.getMaxHealth() - user.getCurrentHealth());
				user.setCurrentHealth(user.getCurrentHealth() + heal);
				cr.addStory("You take a small break to patch yourself up. You heal yourself for %d health points.", heal);
				cr.addStory("You are now at %d/%d health points.", user.getCurrentHealth(), user.getMaxHealth());
			}
		} else if(roll < 0.4f) {
			cr.addStory("You keep walking through the lush forest.");
		} else if(roll < 0.6f) {
			int gold = (int) (5 + 8 * r.nextFloat());
			cr.addStory("You stumble upon the remains of a dead goblin scout.");
			cr.addStory("You find %d gold pieces on his corpse", gold);
			cr.addStory("You continue your journey through the woods.");
		} else if(roll < 0.8f) {
			int dmg = (int) (user.getLevel() * (5 + 3 * r.nextFloat()));
			cr.addStory("You find yourself facing a rapidly-flowing river. You decide to cross it but while doing so, you slip and sprain your ankle, taking %d damage.", dmg);
			user.setCurrentHealth(user.getCurrentHealth() - dmg);
			if(user.getCurrentHealth() <= 0.0f) {
				cr.addStory("While falling, you hit your head on a rock and pass out. Or die. You can't really tell anymore.");
				cr.setPlayerWon(false);
				return;
			}
		} else if(roll < 0.9f) {
			cr.addStory("As you're walking thought the forest, you find a rather tall, mysterious looking statue.");
			cr.addStory("It depicts a dark figure wearing a robe with a hood over its face. The statue is very overgrown and looks very old.");
			cr.addStory("There seems to be something written on its pedestal, but you can't understand the alphabet.");
			cr.addStory("Standing around the statue gives you a strange feeling, and you can't help but feel like you're hearing a faint hum coming from it. You decide it would be wise to leave it be and continue your journey.");
		} else {
			cr.addStory("Some other rare event");
		}
	}
	
	private void resolveMajorEvent(CombatResult cr, User user) {
		Random r = new Random();
		float roll = r.nextFloat();
		
		if(roll < 0.5f) {
			cr.addStory("You stumble upon a Node. Like a small cloud of glowing dust, it's hovering over a bush while producing a relaxing hum.");
			cr.addStory("You touch it gently and its energy enters you. You feel refreshed and somewhat wiser.");
			
			int energyBonus = (int) (2 + 4 * r.nextFloat());
			int xpBonus = (int) (10 + user.getLevel() * 5 * r.nextFloat());
			grantXP(cr, user, xpBonus);
			user.setEnergy(user.getEnergy() + energyBonus);
			cr.addStory("You have gained %d energy points.", energyBonus);
		}
		else {
			// TODO: implement other major events
		}
	}
	
	private Enemy getAppropriateEnemy(User user) {
		Random r = new Random();
		List<Enemy> potentialTargets = enemyBo.findInLevelRange(user.getLevel(), 1);
		if(potentialTargets.size() > 0) {			
			return potentialTargets.get(r.nextInt(potentialTargets.size()));
		}
		return null;
	}
	
	private void grantXP(CombatResult cr, User user, int amount) {
		int oldLevel = user.getLevel();
		user.setXp(user.getXp() + amount);
		int newLevel = user.getLevel();
		
		cr.addStory("You have earned %d xp!", amount);
		if(newLevel > oldLevel) {
			cr.addVictory("You have leveled up!");
			if(newLevel - oldLevel > 1) {
				cr.addStory("%d times, actually! Well done!", newLevel - oldLevel);
			}
		}
	}
}
