package com.siegedog.knights.helpers;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.siegedog.knights.models.user.User;
import com.siegedog.knights.models.user.UserBo;

public class UserHelper {

	public static User getCurrentUser(UserBo userBo) {
		UserDetails ud = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userBo.findByName(ud.getUsername());
	}

	public static boolean isLoggedIn() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails;
	}
}
