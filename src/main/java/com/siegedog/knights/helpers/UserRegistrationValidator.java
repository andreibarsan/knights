package com.siegedog.knights.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.siegedog.knights.models.characterclass.CharacterClassBo;
import com.siegedog.knights.models.user.User;
import com.siegedog.knights.models.user.UserBo;

@Component
public class UserRegistrationValidator implements Validator {

	@Autowired UserBo userBo;
	@Autowired CharacterClassBo ccbo;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		UserRegistration user = (UserRegistration) target;
		if(null == user.getName()) {
			errors.rejectValue("name", "name.null");
		}
		else if(user.getName().length() < 2 || user.getName().length() > 45) {
			errors.rejectValue("name", "name.length");
		}
		
		if(null == user.getPassword() || null == user.getPasswordconf()) {
			errors.reject("passwords.null");
		}
		else if(! user.getPassword().equals(user.getPasswordconf())) {
			errors.reject("passwords.match");
		}
		else if(user.getPassword().length() < 8 || user.getPassword().length() > 250) {
			errors.reject("password.length");
		}
		
		if(null != userBo.findByName(user.getName())) {
			errors.reject("user.nametaken");
		}
		
		if(null == ccbo.findById(user.getCharacterclass())) {
			errors.reject("user.invalidclass");
		}
	}	
}