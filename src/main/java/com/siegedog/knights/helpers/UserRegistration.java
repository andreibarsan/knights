package com.siegedog.knights.helpers;

import org.springframework.stereotype.Component;

@Component
public class UserRegistration {
	private String name;
	private String password;
	private String passwordconf;
	private Long characterclass;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getPasswordconf() {
		return passwordconf;
	}
	
	public void setPasswordconf(String passwordconf) {
		this.passwordconf = passwordconf;
	}
	
	public Long getCharacterclass() {
		return characterclass;
	}

	public void setCharacterclass(Long characterclass) {
		this.characterclass = characterclass;
	}
}