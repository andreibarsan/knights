-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2014 at 08:58 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

USE knights;

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS enemies;
DROP TABLE IF EXISTS mesaj;
DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS items;
DROP TABLE IF EXISTS items_users;
--
-- Database: `knights`
--

-- --------------------------------------------------------

--
-- Table structure for table `enemies`
--

CREATE TABLE IF NOT EXISTS `enemies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  
  -- main stats
  `level` int(11) NOT NULL,
  `max_health` int(11) NOT NULL,
  `attack` int(11) NOT NULL,
  `defense` int(11) NOT NULL,
  
  -- abilties
  heal INT DEFAULT 0,
  weakness INT DEFAULT 0,
  block INT DEFAULT 0,
  
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `enemies`
--

INSERT INTO `enemies` (`name`, `level`, `max_health`, `attack`, `defense`, `heal`, `weakness`, `block`)
VALUES	('Goblin scout', 		1, 		100, 			30, 		50,		0,		0,			0),
		('Sickly kobold', 		2, 		120, 			35, 		40,		0,		1,			0),
		('Goblin spearman', 	3, 		150, 			50, 		60,		0,		0,			0),
		('Goblin swordsman', 	4, 		180, 			65, 		100, 	0,		0,			1),
		('Goblin accountant', 	5, 		180, 			65, 		250,		0,		0,			2),
		('Goblin tribal priest',7, 		200, 			60, 		120,		2,		1,			0),
		('Goblin warrior',		8,		250,			90,			130,		0,		0,			1),
		('Kobold cave crawler',	9,		300,			110,		180,		0,		0,			1),
		('Kobold geomancer',	10,		350,			100,		180,		1,		0,			2),
		('Karagh', 				12, 	550, 			150, 		80,		0,		0,			0),
		('Karagh rider', 		15, 	600, 			200, 		120,		0,		0,			1),
		('Jesus', 				18, 	2000, 			250, 		600,		5,		0,			1);

-- --------------------------------------------------------

--
-- Table structure for table `mesaj`
--

CREATE TABLE IF NOT EXISTS `mesaj` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `sender` BIGINT UNSIGNED NOT NULL,
  `receiver` BIGINT UNSIGNED NOT NULL,
  `msg` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE classes (
	id BIGINT NOT NULL AUTO_INCREMENT,
	name VARCHAR(45) NOT NULL,
	base_attack INTEGER NOT NULL,
	lvl_attack INTEGER NOT NULL,
	base_defense INTEGER NOT NULL,
	lvl_defense INTEGER NOT NULL,
	base_health INTEGER NOT NULL,
	lvl_health INTEGER NOT NULL,
	
	PRIMARY KEY(id)
);

INSERT INTO classes(id, name, base_attack, lvl_attack, base_defense, lvl_defense, base_health, lvl_health)
VALUES 	(100, "Warrior", 10, 6, 10, 4, 100, 45),
		(101, "Paladin", 10, 5, 12, 5, 120, 50),
		(102, "Infiltrator", 15, 6, 10, 3, 80, 40);

CREATE TABLE `users` (
	-- Authentication elements 
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `role` varchar(45) DEFAULT 'ROLE_USER',
  `registered_date` datetime DEFAULT NOW(),
  `password_hash` varchar(64) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  
	-- Gameplay elements
  `energy` int(11) NOT NULL,
  `gold` int(11) NOT NULL,
  `xp` int(11) NOT NULL,
  class BIGINT UNSIGNED NOT NULL,
  
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`name`, `role`, `registered_date`, `password_hash`, `enabled`, `energy`, `gold`, `xp`, `class`) VALUES
('andrei', 'ROLE_USER', '2014-05-17 18:56:11', 'b9f195c5cc7ef6afadbfbc42892ad47d3b24c6bc94bb510c4564a90a14e8b799', 1, 100, 5000, 2500, 100),
('sebi', 'ROLE_USER', '2014-05-17 18:56:11', 'b9f195c5cc7ef6afadbfbc42892ad47d3b24c6bc94bb510c4564a90a14e8b799', 1, 100, 5000, 2500, 101),
('testificate', 'ROLE_USER', '2014-05-17 22:56:11', 'b9f195c5cc7ef6afadbfbc42892ad47d3b24c6bc94bb510c4564a90a14e8b799', 1, 100, 5000, 0, 101);

CREATE TABLE items (
	id SERIAL,
	name VARCHAR(100) NOT NULL,
	description VARCHAR(1024),
	icon VARCHAR(1024),
	
	level INTEGER NOT NULL,
	value INTEGER NOT NULL,
	
	attackbonus INTEGER NOT NULL,
	defensebonus INTEGER NOT NULL,
	healthbonus INTEGER NOT NULL,
	
	PRIMARY KEY(id)
);

INSERT INTO items(id, name, description, level, value, attackbonus, defensebonus, healthbonus) VALUES
	(100, "Stick", "Magical or something", 1, 1000, 100, 0, 0),
	(101, "Helmet", "Looks kind of silly", 3, 500, 0, 50, 0),
	(102, "Sword of hitting", "You can hit stuff with it", 4, 600, 50, 0, 0),
	(103, "Pedantic pendant", "*sigh*", 6, 1500, 0, 0, 100);

CREATE TABLE items_users (
	id SERIAL,
	id_item BIGINT NOT NULL,
	id_user BIGINT NOT NULL,
	
	PRIMARY KEY(id)
);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
